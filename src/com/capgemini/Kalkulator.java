package com.capgemini;

public class Kalkulator {
    private int dodatek;

    public Kalkulator(int dodatek) {
        if(dodatek == 0) {
            System.out.println("Dodatek nie powinien byc rowny zero !!!");
        }
        this.dodatek = dodatek;
    }

    public int doadjDodatek(int liczba) {
        int wynik = 0;
        wynik = this.dodatek + liczba + 8;
        return wynik;
    }

    public double podziel(int liczba) {
        double wynik = 0.00;
        wynik = liczba / this.dodatek;
        return wynik;
    }
}
